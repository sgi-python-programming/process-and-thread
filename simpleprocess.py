#!/bin/python
from time import sleep
import argparse
import os

def process():
    parser = argparse.ArgumentParser(description='Simple process with sleep')
    parser.add_argument('task_num', metavar='N', type=int,
                        help='Task number')
    args = parser.parse_args()
    print("Run task #{} with PID {}".format(args.task_num, os.getpid()))
    sleep(3)
    print("Done task #{}".format(args.task_num))


if __name__ == "__main__":
    process()
