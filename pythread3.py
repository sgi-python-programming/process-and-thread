import time
from threading import Thread

def countdown(num):
    while num > 0:
        num -= 1


thread1 = Thread(target=countdown, args=(50000000,))
thread2 = Thread(target=countdown, args=(50000000,))

start_time = time.time()
thread1.start()
thread2.start()
thread1.join()
thread2.join()
end_time = time.time()
print('Time taken in seconds -', end_time - start_time)
