import os
from multiprocessing import Process
from time import sleep


def print_func(process_num=1, continent='Asia'):
    print('Start function', process_num, 'PID', os. getpid())
    sleep(3) #-process_num)
    print('Process', process_num, 'Continent is : ', continent)


if __name__ == "__main__":  # confirms that the code is under main function
    args = [[1, 'America'], [2, 'Europe'], [3, 'Africa']]
    procs = []

    # instantiating process with arguments
    for arg in args:
        # print(name)
        proc = Process(target=print_func, args=(arg[0], arg[1]))
        procs.append(proc)
        proc.start()

    # complete the processes
    for proc in procs:
        proc.join()