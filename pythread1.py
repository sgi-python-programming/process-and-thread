import time
# from threading import Thread

COUNT = 100000000


def countdown(num):
    while num > 0:
        num -= 1


start_time = time.time()
countdown(COUNT)
end_time = time.time()

print('Time taken in seconds -', end_time - start_time)