import time
from threading import Thread

COUNT = 100000000


def countdown(num):
    while num > 0:
        num -= 1


thread1 = Thread(target=countdown, args=(COUNT // 2,))
thread2 = Thread(target=countdown, args=(COUNT // 2,))

start_time = time.time()
thread1.start()
thread2.start()
thread1.join()
thread2.join()
end_time = time.time()
print('Time taken in seconds -', end_time - start_time)
