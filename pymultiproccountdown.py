import time
import os
from multiprocessing import Process

COUNT = 100000000


def countdown(num):
    while num > 0:
        num -= 1


procs = []
start_time = time.time()
for i in range(2):
    proc = Process(target=countdown, args=(COUNT // 2,))
    procs.append(proc)
    proc.start()

for proc in procs:
    proc.join()
end_time = time.time()
print('Time taken in seconds -', end_time - start_time)
